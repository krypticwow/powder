#define OLC_PGE_APPLICATION
#include "DynArray.h"

class Engine : public olc::PixelGameEngine
{
public:
	Engine() { sAppName = "Powder Toy"; }
	DynArray *Arr;
	float Timer = 0.0f;
	POWDER_ID SelID = POWDER_SAND;

private:
	bool OnUserCreate()
	{
		Arr = new DynArray;
		Arr->Init(150, 150);
		return true;
	}

	bool OnUserUpdate(float fElapsedTime)
	{
		Timer += fElapsedTime;
		Clear(olc::BLACK);
		Arr->Draw(*this);

		if (GetKey(olc::K1).bPressed)
			SelID = POWDER_SAND;
		if (GetKey(olc::K2).bPressed)
			SelID = POWDER_WATER;

		if (GetKey(olc::SPACE).bHeld)
		{
			if (SelID == POWDER_SAND)
			{
				Sand *s = new Sand;
				Arr->Insert(s, GetMousePos());
			}
			else if (SelID == POWDER_WATER)
			{
				Water *w = new Water;
				Arr->Insert(w, GetMousePos());
			}
		}

		if (Timer >= 0.5f)
		{
			Timer = 0.0f;
			Arr->Update();
		}
		return true;
	}
};

int main()
{
	Engine e;
	if (e.Construct(150, 150, 4, 4))
		e.Start();
	return 0;
}