#pragma once
#include "olcPixelGameEngine.h"

enum POWDER_ID
{
	POWDER_NONE = -1,
	POWDER_SAND,
	POWDER_STONE,
	POWDER_WATER,
};

class DynArray;

class Powder
{
protected:
	olc::vi2d Pos;
	POWDER_ID ID;
	float Density;
	bool Moveable = true;
	olc::Pixel Color = olc::RED;

public:
	void SetPos(olc::vi2d pos) { Pos = pos; }
	void Update(DynArray &Arr) {};

	olc::vi2d GetPos() { return Pos; }
	olc::Pixel GetCol() { return Color; }
	float GetDensity() { return Density; }
	POWDER_ID GetID() { return ID; }
};

class Sand : public Powder
{
public:
	Sand() : Powder() { this->Density = 1.0f; this->ID = POWDER_SAND; Color = olc::Pixel(76, 70, 50); }
	
	void Update(DynArray &Arr);
};

class Water : public Powder
{
public:
	Water() { this->Density = 0.5f; this->ID = POWDER_WATER; this->Color = olc::BLUE; }

	void Update(DynArray &Arr);
};