#include "DynArray.h"

void DynArray::Init(int32_t x, int32_t y)
{
	SizeX = x;
	SizeY = y;

	Material = new Powder**[y];

	for (int32_t i = 0; i < SizeY; i++)
		Material[i] = new Powder*[x];

	for (int32_t i = 0; i < SizeY; i++)
		for (int32_t j = 0; j < SizeX; j++)
			Material[i][j] = nullptr;
}

void DynArray::Insert(Powder *pow, olc::vi2d pos)
{
	if (Material[pos.y][pos.x] != nullptr)
		return;

	pow->SetPos(pos);
	Material[pos.y][pos.x] = pow;
}

void DynArray::Swap(olc::vi2d pos1, olc::vi2d pos2)
{
	Powder temp = *Material[pos1.y][pos1.x];

	Material[pos1.y][pos1.x] = Material[pos2.y][pos2.x];
	Material[pos2.y][pos2.x] = &temp;
}

void DynArray::Update()
{
	for (int32_t y = SizeY - 1; y >= 0; y--)
		for (int32_t x = 0; x < SizeX; x++)
			if (Material[y][x] != nullptr)
				Material[y][x]->Update(*this);
}

void DynArray::Draw(olc::PixelGameEngine & e)
{
	for (int32_t y = SizeY - 1; y >= 0; y--)
		for (int32_t x = 0; x < SizeX; x++)
			if (Material[y][x] != nullptr)
				e.Draw({ x, y }, Material[y][x]->GetCol());
}
