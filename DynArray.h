#pragma once
#include "Powder.h"

class DynArray
{
	Powder ***Material;
	int32_t SizeX = 0;
	int32_t SizeY = 0;

public:
	void Init(int32_t x, int32_t y);
	void Insert(Powder *pow, olc::vi2d pos);
	void Swap(olc::vi2d pos1, olc::vi2d pos2);
	void Update();
	void Draw(olc::PixelGameEngine &e);

	Powder *Find(olc::vi2d Pos) { return Material[Pos.y][Pos.x]; }
	Powder *Find(int32_t x, int32_t y) { return Material[y][x]; }

	Powder *operator[](olc::vi2d pos) { return Material[pos.y][pos.x]; }
};

